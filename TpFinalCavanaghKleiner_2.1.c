/*
===============================================================================
 Name        : ProyectoFinalCavanaghKleiner.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include <cr_section_macros.h>

//declaración de variables
static int state=0; //variable que indica el comportamiento del sistema.
static int next_state=0; //indicador del proximo estado.
static int dato1, dato2, dato3, dato4; //contiene la información a mostar en displays,dato4 corresponede a las centenas, dato3 corresponde a decena, dato2 a unidad, dato1 a centavos.
static int disp=0; //que display se prende, centavos=0, unidad=1, decena=2, centena=3.
static unsigned int distancia_total;
//static int auxiliar_vueltas; //las vueltas se incrementan muy rapido y quiero reducir esto
static unsigned int tiempo_parado_total;
static unsigned int aux_uart;
static unsigned int estados_uart=0;
static unsigned int disp_espera;
static float precio_motor_espera = 0.40;
static float precio_metros_recorridos = 5.13;
static float precio_bandera = 22.10;
static unsigned int tiempo_total=0; //cada vez que paso al estado 1, este tiempo se resetea.


//prototipos de función
void configPort(void);
void configTimer(void);
void configUart(void);
void TIMER0_IRQHandler(void);
void logicaDeSalida(void);
int tabla_7Seg(int);
void enviar(char);


int main(void) {
	configPort();
	configTimer();
	configUart();

    while(1) {
    	logicaDeSalida();
    }
    return 0 ;
}
void configPort(void){

	//configuración  del puerto 2.10, como entrada para boton de inicio. EINT0
	LPC_GPIO2->FIODIR &=~(1<<10); //P2.10 como entrada.
	LPC_PINCON->PINSEL4 |=(1<<20);//P2.10 como EINT0.
	LPC_PINCON->PINMODE4 &= ~(3<<20);//por las dudas, pull up activadas (P2.10).
	LPC_SC->EXTMODE |=(1<<0);	//interrumpe por flancos
	LPC_SC->EXTPOLAR &=~(1<<0);	//flanco de bajada genera interrupcion
	NVIC_EnableIRQ(EINT0_IRQn); //habilito int externas P2.10

	//configuración del puerto 2.11 como entrada para boton de stop. ENIT1
	LPC_GPIO2->FIODIR &=~(1<<11); //P2.11 como entrada.
	LPC_PINCON->PINSEL4 |=(1<<22);//P2.11 como EINT1.
	LPC_PINCON->PINMODE4 &= ~(3<<22);//por las dudas, pull up activadas (P2.11).
	LPC_SC->EXTMODE |=(1<<1); //interrumpe por flancos
	LPC_SC->EXTPOLAR &=~(1<<1); //flanco de bajada genera interrupcion
	NVIC_EnableIRQ(EINT1_IRQn); //habilito int externas P2.11

	//configuracion de salidas (P2.0:7) para displays y multiplexación de los mismos(P0.0-2).
	LPC_GPIO2->FIODIR |= (0x3F<<0); //P2.0 a P2.6 como salida.
	LPC_GPIO0->FIODIR |= (0xF<<0); //P0.0, P0.1 , P0.2 y P0.3  como salida.

	//configuración sensor de pasajeros.
	LPC_GPIO0->FIODIR&=~(0xF<<6); //entradas desde el 6 al 9 del puerto 0.

	//configuración salidas para Leds, pasajeros y maxima velocidad.
	LPC_GPIO2->FIODIR |= (1<<7); //led de maxima velocidad como salida. P2.7
	LPC_GPIO2->FIODIR |= (1<<8); //led de pasajeros como salida. P2.8
}

void configTimer(void){
	//configuración timer0 para multiplexado de displays.
	LPC_SC->PCONP|= (1<<1); //energizo timer0.
	LPC_SC->PCLKSEL0 &= ~(3<<2); //cclk/4 como clock del periferico.
	LPC_TIM0->TCR= 0x02; //apago contador y reset contador.
	LPC_TIM0-> MR0 = 125000; //machea en este valor, serian cada 5ms (4 displays ).
	LPC_TIM0-> MCR |= (3<<0); //reset e interrumpe cuando machea.
	NVIC_EnableIRQ(TIMER0_IRQn); //habilito interrupción por timer0.
	LPC_TIM0-> TCR= 0x01; //inicia el contador.

	//configuración timer1 para medir distancia recorrida.
	LPC_SC->PCONP|= (1<<2); //energizo timer1.
	LPC_SC->PCLKSEL0 &= ~(3<<4); //cclk/4 como clock del periferico.
	LPC_TIM1-> TCR= 0x02; //apago contador y reset contador.
	LPC_TIM1-> MR0 = 25000000; //machea en este valor, serian cada 1s.
	LPC_TIM1-> MCR |= (3<<0); //reset e interrumpe cuando machea.

    //configuración timer2, en modo captura de eventos para contar cantidad de vueltas del motor
    LPC_SC->PCONP |= (1<<22); //energizo timer2.
    LPC_SC->PCLKSEL1 &= ~(3<<12); //cclk/4 como clock del periferico.
    LPC_PINCON->PINSEL0|= (3<<8); //p0.4 modo CAP2.0
    LPC_TIM2->TCR =0x02;    //apaga contador y reset contador.
    LPC_TIM2->CTCR |= (2<<0); //incrementa TC con las entradas de CAP.
    LPC_TIM2->CCR |= (2<<0);   //incrementa la cuenta por flanco de bajada.
    //hay que leer valor en LPC_TIM2->CR0
    LPC_TIM2->TCR =0x01; //inicia cuenta
}
void configUart(void){
	//configuracón de UART3.
	LPC_SC->PCONP|=(1<<25); //energizo periferico UART3.
	LPC_SC->PCLKSEL1&=~(0x3<<18); //clock del periferico = cclk/4.
	LPC_UART3->LCR|=(3<<0); //tamaño de palabra de 8 bits
	LPC_UART3->LCR|=(1<<7);	//habilito divisor
	LPC_UART3->DLL= 163; //registro bajo para configuración del BaudRate en 9600 baudios/seg.
	LPC_UART3->DLM= 0;
	LPC_UART3->LCR&=~(1<<7);	//deshabilito divisor
	LPC_UART3->FCR&=~(1<<0); //forzamos a cero fifo, no se utiliza.
	LPC_PINCON->PINSEL1|=(0xFF<<18);//elijo configuracion de tx y rx para pines P0.25(tx), P0.26(rx).
	LPC_UART3->IER|=(1<<0); //habilito interrupción por recepción.
	NVIC_EnableIRQ(UART3_IRQn);//interrupción por UART3 habilitadas.
}

void EINT0_IRQHandler(void){
	for (int i=0; i<70000; i++) {

	}
	if (((((LPC_GPIO2->FIOPIN>>10)&(1))==0) & (((LPC_GPIO0->FIOPIN>>6))&0xF))!=0) {
		if(state==0){
			next_state=1;
			distancia_total=0;
			tiempo_parado_total=0;
			tiempo_total=0;
            //auxiliar_vueltas=0;
			//LPC_TIM1-> TCR= 0x01; //inicia el contador.
			LPC_TIM1-> TCR= 0x02; //apago y reseteo.
			LPC_TIM1-> TCR= 0x01; //inicia el contador.
			NVIC_EnableIRQ(TIMER1_IRQn); //habilito interrupción por timer0.
		}
	}
	LPC_SC->EXTINT |=(1<<0); //bajo interrupción EINT0
}

void EINT1_IRQHandler(void){
	for (int i=0; i<70000; i++) {
	}
	if (((LPC_GPIO2->FIOPIN>>11)&(1))==0) {
		if(state==1){
			next_state=2;
			aux_uart=0;
			disp_espera=0;
		}
		else if(state==2){
			next_state=0; //vuelvo al estado 0.
		}
	}
	LPC_SC->EXTINT |=(1<<1); //bajo interrupción EINT1
}

void TIMER0_IRQHandler(void){
	LPC_GPIO2->FIOSET|=(0x3F<<0);//displays apagados.
	if(disp==0){
		LPC_GPIO0->FIOCLR |=(1<<0); //enciendo displays de centavos.
		LPC_GPIO0->FIOSET |=(1<<1); //apago displays de unidad.
		LPC_GPIO0->FIOSET |=(1<<2); //apago display de decena.
		LPC_GPIO0->FIOSET |=(1<<3);	//apago displey centena.
		LPC_GPIO2->FIOCLR |=(tabla_7Seg(dato1)); //envio que leds se encienden del display unidad.
		disp=1;
	}
	else if(disp==1){
		LPC_GPIO0->FIOCLR |=(1<<1); //enciendo displays de unidad.
		LPC_GPIO0->FIOSET |=(1<<0); //apago display de centavos.
		LPC_GPIO0->FIOSET |=(1<<2);	//apago display de decena.
		LPC_GPIO0->FIOSET |=(1<<3);	//apago displey centena.
		LPC_GPIO2->FIOCLR |=(tabla_7Seg(dato2)); //envio que leds se encienden del display decena.
		disp=2;
	}
	else if(disp==2){
		LPC_GPIO0->FIOCLR |=(1<<2); //enciendo displays de decena.
		LPC_GPIO0->FIOSET |=(1<<0); //apago display de centavos.
		LPC_GPIO0->FIOSET |=(1<<1);	//apago display de unidad.
		LPC_GPIO0->FIOSET |=(1<<3);	//apago displey centena.
		LPC_GPIO2->FIOCLR |=(tabla_7Seg(dato3)); //envio que leds se encienden del display centena.
		disp=0;
	}
	else{
			LPC_GPIO0->FIOCLR |=(1<<3); //enciendo displays de centena.
			LPC_GPIO0->FIOSET |=(1<<0); //apago display de centavos.
			LPC_GPIO0->FIOSET |=(1<<1);	//apago display unidad.
			LPC_GPIO0->FIOSET |=(1<<2);	//apago displey decena.
			LPC_GPIO2->FIOCLR |=(tabla_7Seg(dato4)); //envio que leds se encienden del display centena.
			disp=0;
		}
	LPC_TIM0->IR|=(1<<0); //limpiamos bandera de interrupción
}

void TIMER1_IRQHandler(void){
	if(state==1){
		if ( LPC_TIM2->CR0 > 10 ){           //revisar
			if ( LPC_TIM2->CR0 > 25 ){       //revisar
			    LPC_GPIO2->FIOSET |= (1<<7);	//prender led de velocidad
			}
			else {
				LPC_GPIO2->FIOCLR |= (1<<7);    //apagar led de velocidad
			}
            distancia_total = distancia_total + (int)(LPC_TIM2->CR0/2);
            LPC_TIM2->TCR = 0x02;
            LPC_TIM2->TCR = 0x01;
		}
		else {
			tiempo_parado_total = tiempo_parado_total+1;
		}
		tiempo_total++;
	}
	else if(state==2){
		disp_espera++;
	}
	LPC_TIM1->IR|=(1<<0); //limpiamos bandera de interrupción
}

int tabla_7Seg(int valor)
{
	switch(valor)
	{
	case 0: 	return 0x3F;	//muestra un 0
	case 1:		return 0X06;	//muestra un 1
	case 2:		return 0X5B;	//muestra un 2
	case 3:		return 0X4F;	//muestra un 3
	case 4:		return 0X66;	//muestra un 4
	case 5:		return 0X6D;	//muestra un 5
	case 6:		return 0X7D;	//muestra un 6
	case 7:		return 0X07;	//muestra un 7
	case 8:		return 0X7F;	//muestra un 8
	case 9:		return 0X6F;	//muestra un 9
	}
	return 0;
}

void logicaDeSalida(void){
	if(state==0){
		dato1=0;
		dato2=0;
		dato3=0;
		dato4=0;
	}
	else if(state==1){

		unsigned int precio;
		precio = (tiempo_parado_total*precio_motor_espera) + (distancia_total*(precio_metros_recorridos)/110) + precio_bandera;
		dato1 = (int) ( (precio % 1) * 10);		//guardo los centavos.
		dato2 = (int) ( precio % 10);		    //guardo las unidades.
		dato3 = (int) ( (precio/10) %10);		//guardo las decenas.
		dato4 = (int) ( precio/100);			//guardo las centenas.

	}
	else if (state==2){
		if(aux_uart==0){
			enviar((uint8_t) (tiempo_total & 0xFF));	//enviaremos en principio la parte baja del tiempo total del recorrido.
			enviar((uint8_t) ((tiempo_total>>8) & 0xFF));//enviamos parte alta del dato.
			enviar((uint8_t) (precio & 0xFF));	//enviamos la parte baja.
			enviar((uint8_t) ((precio>>8) & 0xFF));//parte alta.
			aux_uart=1; //solo enviaremos los datos a imprimir una sola vez.
		}
		if(disp_espera>=15){
			next_state=0; //vuelvo al estado inicial.
			LPC_TIM1-> TCR= 0x02; //detengo el contador.
		}
	}

	state=next_state;
}

void UART3_IRQHandler(void){
	if((LPC_UART0->RBR=='m') & (estados_uart==0)){
		estados_uart++;
	}
	if(estados_uart==1){
		precio_motor_espera= (float) LPC_UART0->RBR;
		estados_uart++;
	}
	if(estados_uart==2){
		precio_metros_recorridos= (float) LPC_UART0->RBR;
		estados_uart++;
	}
	if(estados_uart==3){
		precio_bandera= (float) LPC_UART0->RBR;
		estados_uart=0;
	}
}

void enviar(uint8_t c){
	while((LPC_UART3->LSR&(1<<5))==0);
	LPC_UART2->THR = c + 0x30;
}
