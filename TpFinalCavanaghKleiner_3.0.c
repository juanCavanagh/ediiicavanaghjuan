/*
===============================================================================
 Name        : ProyectoFinalCavanaghKleiner.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include <cr_section_macros.h>

//declaración de variables
static int state=0; //variable que indica el comportamiento del sistema.
static int next_state=0; //indicador del proximo estado.
static int dato1, dato2, dato3, dato4; //contiene la información a mostar en displays,dato4 corresponede a las centenas, dato3 corresponde a decena, dato2 a unidad, dato1 a centavos.
static int disp=0; //que display se prende, centavos=0, unidad=1, decena=2, centena=3.
static unsigned int distancia_total;
//static int auxiliar_vueltas; //las vueltas se incrementan muy rapido y quiero reducir esto
static unsigned int tiempo_parado_total;
static unsigned int aux_uart;
static unsigned int estados_uart=0;
static unsigned int disp_espera;
static float precio_motor_espera = 0.80;
static float precio_metros_recorridos = 1.13;
static float precio_bandera = 22.10;
static unsigned int tiempo_total=0; //cada vez que paso al estado 1, este tiempo se resetea.
static uint32_t vueltas_motor=0; //contador de las vultas del motor.
float precio=0;

//prototipos de función
void configPort(void);
void configTimer(void);
void configUart(void);
void TIMER0_IRQHandler(void);
void logicaDeSalida(void);
int tabla_7Seg(int);
void controlPrecioMaximo(void);
void enviarUART(uint8_t c);

void reset(void);

union Data {
   int i;
   float f;
   char str[4];
   uint32_t entero;
} data;

int main(void) {
	configPort();
	configTimer();
	configUart();

    while(1) {
    	logicaDeSalida();
    }
    return 0 ;
}
void configPort(void){

	//configuración  del puerto 2.10, como entrada para boton de inicio. EINT0
	LPC_GPIO2->FIODIR &=~(1<<10); //P2.10 como entrada.
	LPC_PINCON->PINSEL4 |=(1<<20);//P2.10 como EINT0.
	LPC_PINCON->PINMODE4 &= ~(3<<20);//por las dudas, pull up activadas (P2.10).
	LPC_SC->EXTMODE |=(1<<0);	//interrumpe por flancos
	LPC_SC->EXTPOLAR &=~(1<<0);	//flanco de bajada genera interrupcion
	NVIC_EnableIRQ(EINT0_IRQn); //habilito int externas P2.10

	//configuración del puerto 2.11 como entrada para boton de stop. ENIT1
	LPC_GPIO2->FIODIR &=~(1<<11); //P2.11 como entrada.
	LPC_PINCON->PINSEL4 |=(1<<22);//P2.11 como EINT1.
	LPC_PINCON->PINMODE4 &= ~(3<<22);//por las dudas, pull up activadas (P2.11).
	LPC_SC->EXTMODE |=(1<<1); //interrumpe por flancos
	LPC_SC->EXTPOLAR &=~(1<<1); //flanco de bajada genera interrupcion
	NVIC_EnableIRQ(EINT1_IRQn); //habilito int externas P2.11

	//configuración interrupción por EINT2, utilizado para contar vueltas del motor.
    LPC_GPIO2->FIODIR &= ~(1<<12); //P2.12 como entrada
    LPC_PINCON->PINSEL4 |= (1<<24); //modo eint2.
	LPC_PINCON->PINMODE4 &= ~(3<<20);//por las dudas, pull up activadas (P2.10).
    LPC_SC->EXTMODE |=(1<<2);// activo interrupcion
    LPC_SC->EXTPOLAR &= ~(1<<2);	//enit2 por flancos de bajada
    NVIC_EnableIRQ(EINT2_IRQn);

	//configuracion de salidas (P2.0:6) para displays y multiplexación de los mismos(P0.0-2).
	LPC_GPIO2->FIODIR |= (0x7F<<0); //P2.0 a P2.6 como salida.
	LPC_GPIO0->FIODIR |= (0xF<<0); //P0.0, P0.1 , P0.2 y P0.3  como salida.

	//configuración sensor de pasajeros.
	LPC_GPIO0->FIODIR&=~(0xF<<6); //entradas desde el 6 al 9 del puerto 0.

	//configuración salidas para Leds, pasajeros y maxima velocidad.
	LPC_GPIO2->FIODIR |= (1<<7); //led de maxima velocidad como salida. P2.7
	LPC_GPIO2->FIODIR |= (1<<8); //led de pasajeros como salida. P2.8
}

void configTimer(void){
	//configuración timer0 para multiplexado de displays. INTERRUMPE: 5.0ms
	    LPC_SC->PCONP|= (1<<1); //energizo timer0.
	    LPC_SC->PCLKSEL0 &= ~(3<<2); //cclk/4 como clock del periferico.
	    LPC_TIM0->TCR= 0x02; //apago contador y reset contador.
	    LPC_TIM0-> MR0 = 125000; //machea en este valor, serian cada 5ms (4 displays ).
	    LPC_TIM0-> MCR |= (3<<0); //reset e interrumpe cuando machea.
	    NVIC_EnableIRQ(TIMER0_IRQn); //habilito interrupción por timer0.
	    LPC_TIM0-> TCR= 0x01; //inicia el contador.

	//configuración timer1 para medir distancia recorrida. INTERRUMPE: 1.0s
	LPC_SC->PCONP|= (1<<2); //energizo timer1.
	LPC_SC->PCLKSEL0 &= ~(3<<4); //cclk/4 como clock del periferico.
	LPC_TIM1-> TCR= 0x02; //apago contador y reset contador.
	LPC_TIM1-> MR0 = 25000000; //machea en este valor, serian cada 1s.
	LPC_TIM1-> MCR |= (3<<0); //reset e interrumpe cuando machea.

}
void configUart(void){
	//configuracón de UART3. 9600, 1 bit stop, 8 bits de palabra y sin bit paridad.
	LPC_SC->PCONP|=(1<<25); //energizo periferico UART3.
	LPC_SC->PCLKSEL1&=~(0x3<<18); //clock del periferico = cclk/4.
	LPC_UART3->LCR|=(3<<0); //tamaño de palabra de 8 bits
	LPC_UART3->LCR|=(1<<7);	//dlab=1
	LPC_UART3->DLL= 163; //registro bajo para configuración del BaudRate en 9600 baudios/seg.
	LPC_UART3->DLM= 0;
	LPC_UART3->LCR&=~(1<<7);	//dlab=0
	LPC_UART3->FCR&=~(1<<0); //forzamos a cero fifo, no se utiliza.
	 LPC_PINCON->PINSEL1|=(0xF<<18);//elijo configuracion de tx y rx para pines P0.25(tx), P0.26(rx).
	LPC_UART3->IER|=(1<<0); //habilito interrupción por recepción.
	NVIC_EnableIRQ(UART3_IRQn);//interrupción por UART3 habilitadas.
}

void EINT0_IRQHandler(void){
    for (int i=0; i<50000; i++) {} //antirebote
    if (((((LPC_GPIO2->FIOPIN>>10)&(1))==0) & (((LPC_GPIO0->FIOPIN>>6)&0xF)!=0))) { // si despues del for sigue en 1 y ademas hay gente en los asientos
        if(state==0){ //y si estoy en el estado 0
            next_state=1;
            distancia_total=0;
            tiempo_parado_total=0;
            tiempo_total=0;
            LPC_TIM1-> TCR= 0x02; //apago y reseteo.
            LPC_TIM1-> TCR= 0x01; //inicia el contador.
            NVIC_EnableIRQ(TIMER1_IRQn); //habilito interrupción por timer0.
        }
    }
    LPC_SC->EXTINT |=(1<<0); //bajo interrupción EINT0
}

void EINT1_IRQHandler(void){
	for (int i=0; i<70000; i++) {} //anti rebote
	if (((LPC_GPIO2->FIOPIN>>11)&(1))==0) { // si el boton se sigue pulsando despues del antirebote
		if(state==1){ // si estoy en el estado uno
			next_state=2; //paso al 2
			aux_uart=0;
			disp_espera=0;
		}
		else if(state==2){
			next_state=0; //vuelvo al estado 0.
			reset();
		}
	}
	LPC_SC->EXTINT |=(1<<1); //bajo interrupción EINT1
}

void EINT2_IRQHandler(void){
	for (int i=0; i<100; i++) {} //anti rebote
	if(((LPC_GPIO2->FIOPIN>>12)&(1))==0){
		vueltas_motor++;
	}

	LPC_SC->EXTINT|=(1<<2);
}

void TIMER0_IRQHandler(void){
	LPC_GPIO2->FIOSET|=(0x7F<<0);//displays apagados.
	if(disp==0){
		LPC_GPIO0->FIOCLR |=(1<<0); //enciendo displays de centavos.
		LPC_GPIO0->FIOSET |=(1<<1); //apago displays de unidad.
		LPC_GPIO0->FIOSET |=(1<<2); //apago display de decena.
		LPC_GPIO0->FIOSET |=(1<<3);	//apago displey centena.
		LPC_GPIO2->FIOCLR |=(tabla_7Seg(dato1)); //envio que leds se encienden del display unidad.
		disp=1;
	}
	else if(disp==1){
		LPC_GPIO0->FIOCLR |=(1<<1); //enciendo displays de unidad.
		LPC_GPIO0->FIOSET |=(1<<0); //apago display de centavos.
		LPC_GPIO0->FIOSET |=(1<<2);	//apago display de decena.
		LPC_GPIO0->FIOSET |=(1<<3);	//apago displey centena.
		LPC_GPIO2->FIOCLR |=(tabla_7Seg(dato2)); //envio que leds se encienden del display decena.
		disp=2;
	}
	else if(disp==2){
		LPC_GPIO0->FIOCLR |=(1<<2); //enciendo displays de decena.
		LPC_GPIO0->FIOSET |=(1<<0); //apago display de centavos.
		LPC_GPIO0->FIOSET |=(1<<1);	//apago display de unidad.
		LPC_GPIO0->FIOSET |=(1<<3);	//apago displey centena.
		LPC_GPIO2->FIOCLR |=(tabla_7Seg(dato3)); //envio que leds se encienden del display centena.
		disp=3;
	}
	else if(disp==3) {
		LPC_GPIO0->FIOCLR |=(1<<3); //enciendo displays de centena.
		LPC_GPIO0->FIOSET |=(1<<0); //apago display de centavos.
		LPC_GPIO0->FIOSET |=(1<<1);	//apago display unidad.
		LPC_GPIO0->FIOSET |=(1<<2);	//apago displey decena.
		LPC_GPIO2->FIOCLR |=(tabla_7Seg(dato4)); //envio que leds se encienden del display centena.
		disp=0;
		}
	LPC_TIM0->IR|=(1<<0); //limpiamos bandera de interrupción
}

void TIMER1_IRQHandler(void){
	if(state==1){
		if ( vueltas_motor > 20 ){           //revisar
			if ( vueltas_motor > 95 ){       //revisar
			    LPC_GPIO2->FIOSET |= (1<<7);	//prender led de velocidad
			}
			else {
				LPC_GPIO2->FIOCLR |= (1<<7);    //apagar led de velocidad
			}
            distancia_total = distancia_total + (int)((vueltas_motor)/3.6);
		}
		else {
			tiempo_parado_total = tiempo_parado_total+1;
		}
		tiempo_total++;
	}
	else if(state==2){
		disp_espera++;
	}
	vueltas_motor=0;
	LPC_TIM1->IR|=(1<<0); //limpiamos bandera de interrupción
}

int tabla_7Seg(int valor)
{
	switch(valor)
	{
	case 0: 	return 0x3F;	//muestra un 0
	case 1:		return 0X06;	//muestra un 1
	case 2:		return 0X5B;	//muestra un 2
	case 3:		return 0X4F;	//muestra un 3
	case 4:		return 0X66;	//muestra un 4
	case 5:		return 0X6D;	//muestra un 5
	case 6:		return 0X7D;	//muestra un 6
	case 7:		return 0X07;	//muestra un 7
	case 8:		return 0X7F;	//muestra un 8
	case 9:		return 0X6F;	//muestra un 9
	}
	return 0;
}

void logicaDeSalida(void){
	//en este estado no hago nada, muestro display en 0
	if(state==0){
		dato1=0;
		dato2=0;
		dato3=0;
		dato4=0;
	}

	//en este estado voy mostrando el costo acumulado hasta el momento
	else if(state==1){

		precio = (float)(tiempo_parado_total*precio_motor_espera/10.0) + (distancia_total*(precio_metros_recorridos)/110.0) + precio_bandera; // el 110 es porque se cobra metros por segundo
		dato1 = (int)(precio*10) % 10;     //guardo los centavos.
		dato2 = (int) ( (int)precio % 10);           //guardo las unidades.
		dato3 = (int) ( (int)(precio/10) %10);       //guardo las decenas.
		dato4 = (int) ( (int)precio/100);            //guardo las centenas.
	}

	//en este otro estado, ya finalizo el viaje. Mando por uart info del ticket
	else if (state==2){
		if(aux_uart==0){

			//precio del recorrido
			data.f = precio;
			enviarUART( (uint8_t) (data.str[0]));
			enviarUART( (uint8_t) (data.str[1]));
			enviarUART( (uint8_t) (data.str[2]));
			enviarUART( (uint8_t) (data.str[3]));

			//tiempo total del recorrido
			enviarUART( (uint8_t) (tiempo_total & 0xFF) );    //enviaremos en principio la parte baja del tiempo total del recorrido.
			enviarUART( (uint8_t) ( (tiempo_total>>8) & 0xFF) );//enviamos parte alta del dato.

			//envio distancia total reccorido
			data.entero = (uint32_t )distancia_total;
			enviarUART( (uint8_t) (data.str[0]));
			enviarUART( (uint8_t) (data.str[1]));
			enviarUART( (uint8_t) (data.str[2]));
			enviarUART( (uint8_t) (data.str[3]));

			aux_uart=1; //solo enviaremos los datos a imprimir una sola vez.
		}
		else if(disp_espera>=15){
			next_state=0; //vuelvo al estado inicial.
			LPC_TIM1-> TCR= 0x02; //detengo el contador.
			reset();
		}
	}

	//sensor de pasajeros, prende luz en caso de que el taxi se encuentre vacio.
	if(((LPC_GPIO0->FIOPIN>>6)&0xF)!=0){
		LPC_GPIO2->FIOCLR|= (1<<8);	//taxi con pasajeros. Luz apagada.
	}
	else {
		LPC_GPIO2->FIOSET|= (1<<8);	//taxi sin pasajeros. Luz encendida.
	}
	state=next_state;
}

void UART3_IRQHandler(void){

	uint8_t lectura = LPC_UART3->RBR;
	//unicamente puedo configurar los precios si estoy en el estado 0. Para empezar, debo recibir un caracter m.
	if( (lectura=='m') & (estados_uart==0) & (state==0) ){
		estados_uart++;
	}

	else if(estados_uart==1){
		data.str[0]=lectura;
		//precio_motor_espera= (float) (lectura);
		estados_uart++;
	}

	else if(estados_uart==2){
		//precio_metros_recorridos= (float) (lectura);
		data.str[1]=lectura;
		estados_uart++;
	}

	else if(estados_uart==3){
		//precio_bandera= (float) (lectura);
		data.str[2]=lectura;
		estados_uart++;
	}
	else if(estados_uart==4){
		//precio_bandera= (float) (lectura);
		data.str[3]=lectura;
		precio_bandera=data.f;
		estados_uart++;
	}

	else if(estados_uart==5){
		data.str[0]=lectura;
		//precio_motor_espera= (float) (lectura);
		estados_uart++;
	}

	else if(estados_uart==6){
		//precio_metros_recorridos= (float) (lectura);
		data.str[1]=lectura;
		estados_uart++;
	}

	else if(estados_uart==7){
		//precio_bandera= (float) (lectura);
		data.str[2]=lectura;
		estados_uart++;
	}
	else if(estados_uart==8){
		//precio_bandera= (float) (lectura);
		data.str[3]=lectura;
		precio_motor_espera=data.f;
		estados_uart++;
	}

	else if(estados_uart==9){
		data.str[0]=lectura;
		//precio_motor_espera= (float) (lectura);
		estados_uart++;
	}

	else if(estados_uart==10){
		//precio_metros_recorridos= (float) (lectura);
		data.str[1]=lectura;
		estados_uart++;
	}

	else if(estados_uart==11){
		//precio_bandera= (float) (lectura);
		data.str[2]=lectura;
		estados_uart++;
	}
	else if(estados_uart==12){
		//precio_bandera= (float) (lectura);
		data.str[3]=lectura;
		precio_metros_recorridos=data.f;
		estados_uart=0;
		reset();
	}
	//enviar(lectura);

}

void enviarUART(uint8_t c){
	while((LPC_UART3->LSR&(1<<5))==0);
	LPC_UART3->THR = c;
}

void reset(void){
	state=0; //variable que indica el comportamiento del sistema.
	next_state=0; //indicador del proximo estado.
	disp=0; //que display se prende, centavos=0, unidad=1, decena=2, centena=3.
	distancia_total=0;
	tiempo_parado_total=0;
	aux_uart=0;
	estados_uart=0;
	disp_espera=0;
	tiempo_total=0; //cada vez que paso al estado 1, este tiempo se resetea.
	vueltas_motor=0; //contador de las vultas del motor.
	precio=0;
}

